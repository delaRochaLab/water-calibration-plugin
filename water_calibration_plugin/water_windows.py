# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging
import numpy as np

from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlText
from pyforms.controls import ControlButton
from pyforms.controls import ControlCombo
from pyforms.controls import ControlCheckBox
from pyforms.controls import ControlProgress
from pyforms.controls import ControlLabel
from pyforms.controls import ControlList
import water_calibration_plugin.settings as settings
import water_calibration_plugin.data_utils as dut
from water_calibration_plugin.default_values import DefaultValues, ValueLogger
from water_calibration_plugin.control_gui import ControlGui
from pybpodgui_api.models.board import Board
from water_calibration_plugin.exc_manager import log

logger = logging.getLogger(__name__)

class Port:
    """
    A class that contains the state of a single Bpod port during calibration. 
    State implies the state itself, but also the pulse durations, the water results,
    the cycles, the tolerance, the target, etc.
    """

    def __init__(self, port_status, tolerance, cycles, target, pulse_duration, interval):
        self.port_status: bool = port_status
        self.state: str = 'I' if self.port_status else 'disabled'
        self.tolerance: float = float(tolerance)
        self.cycles: int = int(cycles)
        self.target: float = float(target) if target else None
        self.results: tuple = ()
        self.pulse_durations: tuple = (float(pulse_duration),) if pulse_duration else None
        self.interval = float(interval)
        self.result_low: float = None
        self.result_high: float = None
        self.duration_low: float = None
        self.duration_high: float = None

    def __str__(self):
        """
        String representation of a Port instance. Just for debugging purposes.
        """
        return f'{self.port_status}, {self.state}, {self.tolerance}, {self.cycles}, {self.target}, {self.results}, {self.pulse_durations}, {self.interval}, {self.result_low}, {self.result_high}'

    def add_result(self, new_result):
        """
        In the Port variable that contains the state, results are saved in a tuple. This
        method appends to the tuple and converts the input (grams) to uL.
        """
        self.results += (float(new_result) * 1000 / self.cycles, ) if new_result else (None,)

    def add_time(self, new_time):
        """
        In the Port variable, pulse durations are saved in a tuple.
        This method accepts a time in s and appends it.
        """
        self.pulse_durations += (float(new_time),) if new_time else (None,)

    def save_low(self, duration: float, result: float):
        self.duration_low = duration
        self.result_low = result

    def save_high(self, duration: float, result: float):
        self.duration_high = duration
        self.result_high = result


class CalibrationState:
    """
    Helper class which contains the state of the ports during the calibration. Its 
    main component is the board_state dictionary, which contains an array of Port instances
    with the board name as key:
    board_state = {"BPOD05": [Port1, Port2, ...], "BPOD04": [Port1, Port2, ...]}
    The methods are used to obtain data from these ports or to modify them.
    """

    def __init__(self):

        self.board_state = {}
        self.ports_at_start = {} # An array of booleans which registers which ports were enabled at the start

    def add_board(self, board, ports: list):
        self.board_state[board.name] = ports

    def total_ports(self, board):
        return len(self.board_state[board.name])

    def list_ports(self, board):
        """
        A generator that yields all the ports for a given board.
        """
        for port in self.board_state[board.name]:
            yield port

    def latest_times(self, board) -> list:
        """
        Returns the latest obtained pulse_durations for a certain board
        as a list.
        """
        return [port.pulse_durations[-1] for port in self.list_ports(board)]

    def latest_times_str(self, board) -> list:
        """
        Returns the latest obtained pulse_durations for a certain board
        as a list.
        """
        return [str(port.pulse_durations[-1]) for port in self.list_ports(board)]

    def targets(self, board) -> list:
        """
        Returns all the targets for a given board as strings, zero if they were not initialized.
        """
        targets = []
        for port in self.list_ports(board):
            if port.target is not None:
                targets.append(str(port.target))
            else:
                targets.append('0.0')
        return targets

    def latest_grams(self, board) -> list:
        """
        Returns all the water results for a given board as strings, zero if they were not initialized.
        ACTUALLY NOT GRAMS BUT MICROLITERS!
        """
        grams = []
        for port in self.list_ports(board):
            if port.results[-1] is not None:
                grams.append(str(port.results[-1]))
            else:
                grams.append('0.0')
        return grams

    def is_finished(self, board) -> bool:
        """
        Returns True if all the ports have been calibrated.
        """
        states = [True if port.state in {'calibrated', 'disabled'} else False for port in self.list_ports(board)]
        return all(states)
    
    def cycles(self, board):
        return self.board_state[board.name][0].cycles

    def tolerance(self, board):
        return self.board_state[board.name][0].tolerance


class WaterWindow(BaseWidget):
    """
    See:
    - pyforms_generic_editor.models.projects.__init__.py
    - pyforms_generic_editor.models.projects.projects_window.py
    """
    @log
    def __init__(self, projects):

        BaseWidget.__init__(self, 'Water Calibration', parent_win=projects.mainwindow)
        self.projects = projects
        self.set_margin(10)
        # Class to control the GUI:
        self.controlGui = ControlGui(self)
        # Variable that will contain the calibration state throughout the calibration:
        self.calibration_state = CalibrationState()
        # Array that contains the connected boards (as Board objects):
        self.connected_boards = self.controlGui.boards

        form = []
        tabs = {}

        # Main loop to generate the form programatically:
        for (index, board) in enumerate(self.connected_boards):
            # Fetch defaults:
            defaults = DefaultValues(board).data
            tabs[board.name] = []
            # Create the interval, cycle and tolerance prompts:
            setattr(self, f"_interval_{index}",
                    ControlText('Interval (s): ',
                    default="0.3",
                    helptext='Time interval between pulses in seconds.'))
            setattr(self, f"_cycles_{index}",
                    ControlText('No. of pulses: ',
                    default=str(defaults['cycles']),
                    helptext='How many times the valves are opened.'))
            setattr(self, f"_tolerance_{index}",
                    ControlText('Tolerance (±uL): ',
                    default=str(defaults['tolerance']),
                    helptext='Desired tolerance of the calibration, in uL.'))
            tabs[board.name].append((f"_cycles_{index}",
                                    f"_interval_{index}",
                                    f"_tolerance_{index}"))
            # Run, stop and send buttons:
            setattr(self, f"_runButton_{index}",
                    ControlButton('Run',
                    helptext='Start the calibration procedure.'))
            setattr(self, f"_stopButton_{index}",
                    ControlButton('Stop',
                    helptext='Stop the calibration procedure.',
                    enabled=False))
            setattr(self, f"_sendButton_{index}",
                    ControlButton('Send',
                    helptext='Send the calibration results to continue.',
                    enabled=False))
            # Assign the functions to the buttons:
            getattr(self, f"_stopButton_{index}").value = getattr(self, f"_stopTask_{index}")
            getattr(self, f"_runButton_{index}").value = getattr(self, f"_runTask_{index}")
            getattr(self, f"_sendButton_{index}").value = getattr(self, f"_send_result_{index}")
            tabs[board.name].append((" ", f"_runButton_{index}", f"_stopButton_{index}",
                                    f"_sendButton_{index}", " "))
            # Prompts for each port. First get the status (enabled or not):
            port_status = self.controlGui.active_ports(board.name)
            # Save the initial port states so that they can be recovered at the end:
            self.calibration_state.ports_at_start[board.name] = port_status
            if port_status is None: # Board is detected but not connected, so active_ports returns None
                port_status = [False] * 8
            for (valve, port_status) in zip(range(1,9), port_status):
                try:
                    default_target = str(defaults['target'][valve-1])
                except:
                    default_target = '0.0'
                try:
                    default_pulse = str(defaults['pulse_duration'][valve-1])
                except:
                    default_pulse = '0.0'

                # Checkboxes to enable and disable ports:
                setattr(self, f"_enable_{index}_{valve}",
                                ControlCheckBox('',
                                helptext='Enable or disable the port.',
                                value = True))
                setattr(self, f"_target_{index}_{valve}",
                            ControlText('Target (uL): ',
                            default=default_target,
                            helptext='The target calibration value in microliters.'))
                setattr(self, f"_time_{index}_{valve}",
                            ControlText('Time (s): ',
                            default=default_pulse,
                            helptext='The valve pulse time in seconds.'))
                setattr(self, f"_result_{index}_{valve}",
                            ControlText('Result (g): ',
                            helptext='The scale measurement of the water weight, in grams.'))
                state = 'active' if port_status else 'disabled'
                setattr(self, f"_state_{index}_{valve}",
                            ControlText('',
                            default=state,
                            helptext='Calibration state of the port.'))
                # Disable all time and state prompts (they are only for display):
                getattr(self, f"_time_{index}_{valve}").enabled = False
                getattr(self, f"_state_{index}_{valve}").enabled = False
                # Disable those ports which are inactive:
                if not port_status:
                    getattr(self, f"_enable_{index}_{valve}").enabled = False
                    getattr(self, f"_target_{index}_{valve}").enabled = False
                    getattr(self, f"_result_{index}_{valve}").enabled = False
                tabs[board.name].append((f"_enable_{index}_{valve}",
                                        f"Port {valve}:",
                                        f"_target_{index}_{valve}",
                                        f"_time_{index}_{valve}",
                                        f"_result_{index}_{valve}",
                                        f"_state_{index}_{valve}"))

        form.append(tabs)
        # Send the finished form to pyforms:
        self.formset = form

    @log
    def calibration(self, board):
        """
        Contains the state machine that performs the calibration.
        """
        
        DECIMALS = 4

        def linear(duration, target, result) -> float:

            new_duration = round(duration * target / result, DECIMALS)

            print('linear from duration', duration, 'result', result)
            print('new duration', new_duration)

            return new_duration

        def calibration_ended(target: float, tolerance: float , result: float) -> bool:
            return float(tolerance) > abs(target-result)

        def time_bisection(port, state_type=None) -> float:

            result1 = port.results[-1]
            result2 = port.results[-2]
            duration1 = port.pulse_durations[-1]
            duration2 = port.pulse_durations[-2]
            result_target = port.target

            if state_type == 'low':
                result2 = port.result_low
                duration2 = port.duration_low
            elif state_type == 'high':
                result2 = port.result_high
                duration2 = port.duration_high

            bisect = (result_target - result1) * (duration2 - duration1) / (result2 - result1) + duration1
            new_duration = round(bisect, DECIMALS)

            print('bisect between duration1', duration1, 'result1', result1, 'duration2', duration2, 'result2', result2)
            print('new duration', new_duration)

            return new_duration

        # State machine for the calibration procedure, acts on each port of the board:
        for port in self.calibration_state.list_ports(board):
            state = port.state
            if port.port_status and state != 'calibrated':
                # Last received value for that port, already in uL:
                result_ul = port.results[-1]
                # Last pulse_duration, in s:
                pulse_duration = port.pulse_durations[-1]

                print('duration', pulse_duration, 'result', result_ul)

                if result_ul != 0:
                    if state == 'I':
                        if calibration_ended(port.target, port.tolerance, result_ul):
                            port.state = 'calibrated'
                        elif port.target > result_ul:
                            port.state = 'lower'
                            port.save_low(pulse_duration, result_ul)
                            port.add_time(round(linear(pulse_duration, port.target, result_ul), DECIMALS))
                            # TODO: add top value
                        else:
                            port.state = 'higher'
                            port.save_high(pulse_duration, result_ul)
                            new_time = linear(pulse_duration, port.target, result_ul)
                            if new_time <= 0:
                                self.warning(f'Minimum calibration value reached (0.001) at one of the ports. Please check the measurements.', "Warning")
                                new_time = 0.001
                            port.add_time(round(new_time, DECIMALS))
                    elif state == 'lower':
                        if calibration_ended(port.target, port.tolerance, result_ul):
                            port.state = 'calibrated'
                        elif port.target > result_ul:
                            port.state = 'lower'
                            port.save_low(pulse_duration, result_ul)
                            port.add_time(round(linear(pulse_duration, port.target, result_ul), DECIMALS))
                            # TODO: add top value
                        else:
                            port.state = 'SHL'
                            port.save_high(pulse_duration, result_ul)
                            port.add_time(time_bisection(port))
                    elif state == 'SHL':
                        if calibration_ended(port.target, port.tolerance, result_ul):
                            port.state = 'calibrated'
                        elif port.target > result_ul:
                            port.state = 'SLH'
                            port.save_low(pulse_duration, result_ul)
                            port.add_time(time_bisection(port))
                            # TODO: add top value
                        else:
                            port.state = 'SHL!'
                            port.save_high(pulse_duration, result_ul)
                            port.add_time(time_bisection(port, 'low'))
                    elif state == 'SHL!':
                        if calibration_ended(port.target, port.tolerance, result_ul):
                            port.state = 'calibrated'
                        elif port.target > result_ul:
                            port.state = 'SLH'
                            port.save_low(pulse_duration, result_ul)
                            port.add_time(time_bisection(port))
                            # TODO: add top value
                        else:
                            port.state = 'SHL!'
                            port.save_high(pulse_duration, result_ul)
                            port.add_time(time_bisection(port, 'low'))
                    elif state == 'SLH':
                        if calibration_ended(port.target, port.tolerance, result_ul):
                            port.state = 'calibrated'
                        elif port.target > result_ul:
                            port.state = 'SLH!'
                            port.save_low(pulse_duration, result_ul)
                            port.add_time(time_bisection(port, 'high'))
                            # TODO: add top value
                        else:
                            port.state = 'SHL'
                            port.save_high(pulse_duration, result_ul)
                            port.add_time(time_bisection(port))
                    elif state == 'SLH!':
                        if calibration_ended(port.target, port.tolerance, result_ul):
                            port.state = 'calibrated'
                        elif port.target > result_ul:
                            port.state = 'SLH!'
                            port.save_low(pulse_duration, result_ul)
                            port.add_time(time_bisection(port, 'high'))
                            # TODO: add top value
                        else:
                            port.state = 'SHL'
                            port.save_high(pulse_duration, result_ul)
                            port.add_time(time_bisection(port))
                    elif state == 'higher':
                        if calibration_ended(port.target, port.tolerance, result_ul):
                            port.state = 'calibrated'
                        elif port.target > result_ul:
                            port.state = 'SLH'
                            port.save_low(pulse_duration, result_ul)
                            port.add_time(time_bisection(port))
                            # TODO: add top value
                        else:
                            port.state = 'higher'
                            port.save_high(pulse_duration, result_ul)
                            new_time = linear(pulse_duration, port.target, result_ul)
                            if new_time <= 0:
                                self.warning(f'Minimum calibration value reached (0.001) at one of the ports. Please check the measurements.', "Warning")
                                new_time = 0.001
                            port.add_time(round(new_time, DECIMALS))

        number = self.find_board_index(board)
        # Update the state information displays:
        for index, port in enumerate(self.calibration_state.list_ports(board), 1):
            print(port)
            if port.state not in ('calibrated', 'disabled'):
                state_text = 'running'
            else:
                if port.state == 'calibrated':
                    getattr(self, f"_result_{number}_{index}").enabled = False
                state_text = port.state
            getattr(self, f"_state_{number}_{index}").value = state_text
            getattr(self, f"_time_{number}_{index}").value = str(self.calibration_state.latest_times(board)[index-1])

        # If calibration has ended, save the data and disable the run button
        # to avoid saving twice. 
        if self.calibration_state.is_finished(board):
            logger = ValueLogger(board)
            logger.save_data(board, '-'.join(self.calibration_state.targets(board)),
                    '-'.join(self.calibration_state.latest_times_str(board)), 
                    '-'.join(self.calibration_state.latest_grams(board)),
                    self.calibration_state.cycles(board),
                    self.calibration_state.tolerance(board))
            self.stop_task(number)
            # Save the project:
            self.controlGui._project.save()
        else:
            # ---- UPDATE VARIABLES ----
            times_vector = self.calibration_state.latest_times(board)
            times_vector_str = '-'.join([str(elem) for elem in times_vector])
            # Pulse duration variable is #2 inside the task:
            self.controlGui.modify_variable(2, times_vector_str)
            # ---- RUN TASK AGAIN ----
            self.run(board)

    @log
    def stop_task(self, number: int):
        """
        Stops the task without saving the data. It clears the calibration state variable
        and resets the buttons to the initial value and resets ports to initial values.
        """
        board = self.connected_boards[number]
        # Reset buttons and displays to original state:
        self.start_buttons(number)
        # Wipe out any non-finished calibrations:
        if board.name in self.calibration_state.board_state:
            self.calibration_state.board_state.pop(board.name)
        # Return the board to its initial state (enabled and disabled ports):
        # (this is also done after finishing a calibration or closing the plugin window)
        board.enabled_behaviorports = self.calibration_state.ports_at_start[board.name]
        self.controlGui._project.save()

    @log
    def run(self, board):
        """
        Goes to the correct setup and starts the task. The calibration method
        calls this method upon finishing, provided that the calibration is 
        not finished.
        """
        try:
            self.controlGui.run_task()
        except Exception:
            index = self.find_board_index(board)
            self.start_buttons(index)

    @log
    def run_check(self, number: int):
        """
        Does a sanity check on the start variables (target, cycle, interval and tolerance):
        no blanks, no spaces, no letters, no commas. Only ints/floats with points as decimal
        separators.
        """
        board = self.connected_boards[number]
        # Check tolerance, cycles and interval:
        tolerance = getattr(self, f'_tolerance_{number}').value 
        cycles = getattr(self, f'_cycles_{number}').value         
        interval = getattr(self, f'_interval_{number}').value         
        fields = {'tolerance': tolerance, 'cycles': cycles, 'interval': interval}
        for field in fields:
            if field == 'cycles':
                try:
                    int(fields[field])
                except ValueError:
                    self.warning(f'There is an error in the {field} field.\nIt can only be an integer.', 'Warning')
                    return False
                if int(fields[field]) <= 0:
                    self.warning(f'There is an error in the {field} field.\nIt cannot be zero or negative.', 'Warning')
                    return False
            else:
                try:
                    float(fields[field])
                except ValueError:
                    self.warning(f'There is an error in the {field} field.\nOnly floats with dots as separators are allowed!', 'Warning')
                    return False
                if float(fields[field]) <= 0:
                    self.warning(f'There is an error in the {field} field.\nIt cannot be zero or negative.', 'Warning')
                    return False

        for valve, port in enumerate(self.controlGui.active_ports(board.name), 1):
            if port:
                target = getattr(self, f'_target_{number}_{valve}').value
                try:
                    float(target)
                except ValueError:
                    self.warning(f"There is an error in the target of port {valve}.\nOnly floats with dots as separators are allowed!", 'Warning')
                    return False
                if float(target) <= 0:
                    self.warning(f"There is an error in the target of port {valve}.\nIt cannot be zero or negative.", 'Warning')
                    return False

        return True
                
    @log
    def pre_run_task(self, number: int):
        """
        Each run button will call this pre-initialization method. It disables the Run button
        and enables the Send and Stop buttons; gets the correct setup for that board;
        populates the calibration state variable with the Ports and runs the calibration task.
        """
        # Fetch board and total number of connected ports:
        board = self.connected_boards[number]
        port_num = len(self.controlGui.active_ports(board.name))
        # Check the enable/disable checkboxes:
        for valve in range(1, port_num+1):
            if not getattr(self, f'_enable_{number}_{valve}').enabled:
                continue
            elif not getattr(self, f'_enable_{number}_{valve}').value:
                self.disable_port(number, valve)
        # Check if the variables are properly set-up:
        if self.run_check(number):
            # Enable/disable buttons:
            getattr(self, f"_stopButton_{number}").enabled = True
            getattr(self, f"_sendButton_{number}").enabled = True
            getattr(self, f"_runButton_{number}").enabled = False
            # Create the setup for this board:
            self.controlGui._check_setup(board)
            self.controlGui.modify_board(board)
            #self.controlGui._check_variables()
            # Add the cycle, interval and pulse_duration variables:
            self.controlGui.modify_variable(0, getattr(self, f"_interval_{number}").value)
            self.controlGui.modify_variable(1, getattr(self, f"_cycles_{number}").value)
            defaults  = DefaultValues(board).data
            pulse_durations = [getattr(self, f'_time_{number}_{jj}').value for jj in range(1, port_num+1)]
            pulse_durations = "-".join(pulse_durations)
            print(pulse_durations)
            self.controlGui.modify_variable(2, pulse_durations)
            # Fetch port status:
            port_status = self.controlGui.active_ports(board.name)
            # Create the ports list with Port instances:
            ports = []
            for index in range(1, port_num + 1):
                ports.append(Port(port_status[index-1],
                                  getattr(self, f"_tolerance_{number}").value,
                                  getattr(self, f"_cycles_{number}").value,
                                  getattr(self, f"_target_{number}_{index}").value,
                                  getattr(self, f"_time_{number}_{index}").value,
                                  getattr(self, f"_interval_{number}").value))
                # Disable the target buttons during calibration:
                getattr(self, f'_target_{number}_{index}').enabled = False
            self.calibration_state.add_board(board, ports)
            # Tell the board to run the task:
            self.controlGui._project.save()
            self.run(board)

    @log
    def send_check(self, number: int):
        board = self.connected_boards[number]
        for valve, port in enumerate(self.controlGui.active_ports(board.name), 1):
            if port:
                result = getattr(self, f'_result_{number}_{valve}').value
                try:
                    float(result)
                except ValueError:
                    self.warning(f"There is an error in the result of port {valve}.\nOnly floats with dots as separators are allowed!", 'Warning')
                    return False
                if float(result) < 0:
                    self.warning(f"There is an error in the result of port {valve}.\nIt cannot be negative.", 'Warning')
                    return False

        return True

    @log
    def general_send_result(self, number: int):
        if self.send_check(number):
            board = self.connected_boards[number]
            self.controlGui.change_setup(board)
            self.controlGui._check_variables()
            self.controlGui.modify_variable(0, getattr(self, f"_interval_{number}").value)
            for index, port in enumerate(self.calibration_state.list_ports(board), 1):
                port.add_result(getattr(self, f"_result_{number}_{index}").value)
            self.calibration(board)
        
    # Functions attached to the send button:
    def _send_result_0(self):
        self.general_send_result(0)
    def _send_result_1(self):
        self.general_send_result(1)
    def _send_result_2(self):
        self.general_send_result(2)
    def _send_result_3(self):
        self.general_send_result(3)
    def _send_result_4(self):
        self.general_send_result(4)
    def _send_result_5(self):
        self.general_send_result(5)
    def _send_result_6(self):
        self.general_send_result(6)
    def _send_result_7(self):
        self.general_send_result(7)

    # Functions attached to the run button:
    def _runTask_0(self):
        self.pre_run_task(0)
    def _runTask_1(self):
        self.pre_run_task(1)
    def _runTask_2(self):
        self.pre_run_task(2)
    def _runTask_3(self):
        self.pre_run_task(3)
    def _runTask_4(self):
        self.pre_run_task(4)
    def _runTask_5(self):
        self.pre_run_task(5)
    def _runTask_6(self):
        self.pre_run_task(6)
    def _runTask_7(self):
        self.pre_run_task(7)

    # Functions attached to the stop button:
    def _stopTask_0(self):
        self.stop_task(0)
    def _stopTask_1(self):
        self.stop_task(1)
    def _stopTask_2(self):
        self.stop_task(2)
    def _stopTask_3(self):
        self.stop_task(3)
    def _stopTask_4(self):
        self.stop_task(4)
    def _stopTask_5(self):
        self.stop_task(5)
    def _stopTask_6(self):
        self.stop_task(6)
    def _stopTask_7(self):
        self.stop_task(7)
        
    # ---------- HELPER FUNCTIONS -------------------
    def start_buttons(self, number):
        """
        Resets the buttons to the original state: run enabled, send and stop disabled.
        It also resets the display.
        """
        board = self.connected_boards[number]
        port_num = len(self.controlGui.active_ports(board.name))
        getattr(self, f"_runButton_{number}").enabled = True
        getattr(self, f"_sendButton_{number}").enabled = False
        getattr(self, f"_stopButton_{number}").enabled = False
        defaults = DefaultValues(board).data
        # Enable the displays again:
        for valve in range(1, port_num + 1):
            if self.calibration_state.ports_at_start[board.name][valve-1]:
                getattr(self, f"_target_{number}_{valve}").enabled = True
                getattr(self, f"_result_{number}_{valve}").enabled = True
                getattr(self, f"_result_{number}_{valve}").value = ''
                getattr(self, f"_enable_{number}_{valve}").enabled = True
                getattr(self, f"_time_{number}_{valve}").value = defaults['pulse_duration'][valve-1]

    def find_board_index(self, board):
        """
        Give it a board and it will find its number inside the connected_boards
        array.
        """
        for index, board_element in enumerate(self.connected_boards):
            if board_element.name == board.name:
                return index

    def disable_port(self, board_int: int, port_int: int):
        """
        Disables port in the board and sets the display to false.
        """
        board = self.connected_boards[board_int] 
        list_ports = board.enabled_behaviorports
        list_ports[port_int-1] = False
        board.enabled_behaviorports = list_ports
        getattr(self, f"_target_{board_int}_{port_int}").enabled = False
        getattr(self, f"_result_{board_int}_{port_int}").enabled = False
        getattr(self, f"_result_{board_int}_{port_int}").value = ''
        getattr(self, f'_enable_{board_int}_{port_int}').enabled = False
        getattr(self, f'_state_{board_int}_{port_int}').value = 'disabled'
     
    def beforeClose(self):
        return False

    @log
    def before_close_event(self):
        """
        Before closing the plugin, restore the ports again (just in case) 
        and reset the variables.
        """
        for number, board in enumerate(self.connected_boards):
            board.enabled_behaviorports = self.calibration_state.ports_at_start[board.name]
            if self.calibration_state.board_state:  
                self.stop_task(number)

        self.controlGui._project.save()

    @property
    def mainwindow(self):
        return self.projects.mainwindow

