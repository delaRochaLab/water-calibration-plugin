# !/usr/bin/python3
# -*- coding: utf-8 -*-

import water_calibration_plugin.settings as settings
import csv
import datetime

class DefaultValues:
    """
    This class receives a board and grabs its calibration data (CalData) at the start of the plugin.
    If the calibration data is not present, it loads some default values.
    """
    def __init__(self, board: str):
        logger = ValueLogger(board.name)
        self.data = logger.load_latest(board)
        # These are the defaults for those boards which were never calibrated:
        if self.data is None:
            if board.enabled_behaviorports is not None:
                port_num = len(board.enabled_behaviorports)
            else:
                port_num = 8
            if port_num < 5:
                pulse = 0.005
            else:
                pulse = 0.05
            self.data = {}
            self.data['target'] = [24] * port_num  # Calibration target in uL for every port
            self.data['tolerance'] = 0.5  # Port tolerance in uL
            self.data['pulse_duration'] = [pulse] * port_num  # Pulse duration for every port in ms
            self.data['cycles'] = 100  # Number of iterations

class ValueLogger:
    """
    This class saves the calibration data into a csv file for persistence between
    calibration sessions.
    """
    def __init__(self, board):
        self.board = board
        
    def load_latest(self, board):
        with open(settings.WATER_PLUGIN_FILE_PATH, 'r') as log:
            calibration_data = csv.DictReader(log, delimiter=';')
            latest_value = None
            for row in calibration_data:
                if row['board'] == board.name:
                    latest_value = dict(row)
        # These are the defaults for those board who are already saved in the CSV:
        if latest_value is not None:
            if board.enabled_behaviorports is not None:
                port_num = len(board.enabled_behaviorports)
            else:
                port_num = 8
            if port_num < 5:
                pulse = 0.005
            else:
                pulse = 0.05
            latest_value['target'] = latest_value['target'].split('-')
            latest_value['pulse_duration'] = latest_value['pulse_duration'].split('-')
            latest_value['pulse_duration'] = [pulse if elem == '0.0' else elem for elem in latest_value['pulse_duration']]
            latest_value['pulse_duration'] = [str(round(float(pulse), 4)) for pulse in latest_value['pulse_duration']]
            latest_value['cycles'] = int(float(latest_value['cycles']))
        return latest_value

    def save_data(self, board, targets, pulses, water, cycles, tolerance):
        data = [board.name, datetime.datetime.now(), targets, pulses, water, cycles, tolerance]
        with open(settings.WATER_PLUGIN_FILE_PATH, 'a') as log:
            record = csv.writer(log, delimiter=';')
            record.writerow(data)

