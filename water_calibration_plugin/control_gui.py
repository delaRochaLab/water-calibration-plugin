# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" Class useful to contain all the method that interacs with the GUI"""
import water_calibration_plugin.settings as settings
from water_calibration_plugin.default_values import DefaultValues, ValueLogger

class ControlGui:

    def __init__(self, gui):
        #self.gui = gui
        self._gui          = gui
        self._project      = None
        self._boards       = None
        self._boardIdx     = None
        self._experiment   = None
        self._setup        = None
        self._task         = None
        self._variables    = None
        self.check()

    def check(self):
        """ Check all the parameters """
        self.__check_project()
        self.__check_board()
        self.__check_task()
        self.__check_experiment()
        #self.__check_setup()
        #self.__check_variables()

        # Assign the protocol
        #self._setup.task = self._task

    def stopBpod(self):
        self._setup.stop_task()

    def boardStatus(self):
        return self._boards[self._boardIdx].status

    def enable_port(self, port):
        list_ports = self._boards[self._boardIdx].enabled_behaviorports
        list_ports[port] = True
        self._boards[self._boardIdx].enabled_behaviorports = list_ports

    def disable_port(self, port):
        list_ports = self._boards[self._boardIdx].enabled_behaviorports
        list_ports[port] = False
        self._boards[self._boardIdx].enabled_behaviorports = list_ports

    @property
    def has_boards(self):
        """ Check if there is at least one board"""
        return len(self._boards) > 0 

    @property
    def boards(self):
        return self._boards
    
    def active_ports(self, board_name: str):
        if self.has_boards:
            for board in self.boards:
                if board_name == board.name:
                    return board.enabled_behaviorports
        else:
            return None

    def find_board(self, number: int):
        if self.has_boards:
            for board in self.boards:
                print(board.name.split()[-1])
                print(int(board.name.split()[-1]))
                if int(board.name.split()[-1]) == number:
                    return board
            else:
                return None

    def add_variable(self, name: str, value: str):
        """ Modify a value of a variable """
        self._variables.append(self._setup.board_task.create_variable(name, value))
        self.__saveProject()

    def modify_variable(self, idx, value):
        """ Modify a value of a variable """
        self._setup.board_task.variables[idx].value = str(value)
        self.__saveProject()

    def modify_board(self, value):
        """ Modify the board of the setup """
        self.__check_board()
        self._setup.board = value
        self.__saveProject()

    def run_task(self):
        """ Run Bpod """
        self._setup.run_task()

    def __check_task(self):
        """ Check if the tast exists"""
        found = False
        for t in self._project.tasks: # Try to find it
            if t.name == settings.WATER_PLUGIN_NAME_TASK:
                self._task = t
                found = True
                break

        if self._task is None or not found: # Create it
            self._task = self._project.import_task(settings.WATER_PLUGIN_PATH_TASK)
            self._task.hide()

    def __check_experiment(self):
        """ Check if the experiment exists"""
        found = False
        for e in self._project.experiments: # Try to find it
            if e.name == settings.WATER_PLUGIN_NAME_EXPERIMENT:
                self._experiment = e
                found = True
                break

        if self._experiment is None or not found: # Create the experiment and its setup
            # Create the experiment
            self._experiment = self._project.create_experiment()
            self._experiment.hide()
            self._experiment.name = settings.WATER_PLUGIN_NAME_EXPERIMENT
            
            # Create the setup
            #self._setup = self._experiment.create_setup()
            #self._setup.name = settings.WATER_PLUGIN_NAME_SETUPS
            #self.__createVariables()

    def change_setup(self, board):
        for s in self._experiment.setups: # Try to find it
            if s.name == f"calibration_{board.name}":
                self._setup = s
                break
        
    def _check_setup(self, board):
        print(f'board: {board.name}')
        """ Check if the setup exists"""
        found = False
        for s in self._experiment.setups: # Try to find it
            if s.name == f"calibration_{board.name}":
                self._setup = s
                found = True
                break
        if self._setup is None or not found: # Create it
            # Create the setup
            self._setup = self._experiment.create_setup()
            self._setup.name = f"calibration_{board.name}"
            self.__createVariables(board)
        # Check variables
        if len(self._setup.board_task.variables) == 0:
            self.__createVariables(board)
        self._setup.task = self._task

    def _check_variables(self):
        if self._variables is None:
            self._variables = []
            for v in self._setup.board_task.variables:
                self._variables.append(v)

    def __createVariables(self, board):
        """
        If variables have to be created, create three dummy vars that will be updated 
        when the user clicks "Run".
        """
        self._variables = []
        # Var_1: interval
        self._variables.append(self._setup.board_task.create_variable('VAR_INTERVAL', '0'))
        # Var_2: iterations to do 
        self._variables.append(self._setup.board_task.create_variable('VAR_ITER', '0'))
        # Var_3: timer valves
        self._variables.append(self._setup.board_task.create_variable('VAR_DUR', '0'))

    def __check_board(self):
        self._boards  = self._project.boards

    def __check_project(self):
        self._project = self._gui.projects.projects[0]

    def __saveProject(self):
        self._project.save()

    def is_port_open(self, port):
        self.__find_boardIdx()
        return self._boards[self._boardIdx].enabled_behaviorports[port]

    def __find_boardIdx(self):
        for i in range(len(self._boards)):
            if self._boards[i] == self._setup.board:
                self._boardIdx = i
                break

