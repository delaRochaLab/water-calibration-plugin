# Water system calibration plugin #

A plugin for [Pyforms Generic Editor Welcome plugin](https://bitbucket.org/fchampalimaud/pybpod-gui-plugin).
Each one of the boxes in the setup uses up to three valves to deliver water to the animals, and the amount of water has to be 
controlled accurately to around some tens of microliters (depending on the task and whether they are rats or mice). 
This plugin eases the calibration task by providing a visual aid to the technician. The calibration procedure is really straighforward:
a precision scale is used to measure the amount of water coming out of the valves when a known pulse is used as the trigger. The amount of delivered 
water is compared with the target and the duration of the pulse is adjusted accordingly until we reach the target. 

Since the water valves are connected directly to the Bpod device, the easiest way to do the calibration is by using an automatic test task
(see `water_calibration_task.py`) which basically turns on and off the valves at the desired rate. Many different boxes can be calibrated at the same time.
The way the plugin helps to achieve the calibration is as follows:

1. Upon opening the plugin, the window should show detect the number of ports (4 for 2.0 Bpods, 8 for 1.0 and below) and whether they are enabled or disabled.
The upper window should have tabs corresponding to all the connected Bpods. Each tab is independent from each other.
2. The technician sets the desired calibration parameters on the plugin window: number of pulses (usually 100-300), interval between
pulses (beware: if the interval is set too low, less than 0.5 s, Bpod tends to go crazy and gives the green light of death at the middle of the task)
and the desired tolerance in microliters. The ports to be calibrated can also be selected using the checkboxes.
3. A container for the water such as a small vase is put below the valves, and the task is run. Water will start pouring on the vase.
4. When the task finishes, the water weight minus the weight of the empty container is put into the `Result` column in grams. The containers are emptied
and the technician hits `Send` to run the task again: the program will adjust the pulse times and the cycle starts again.
5. Each port will be disabled as the calibration is reached, until all ports are calibrated.
6. At the end, a new entry is saved into the calibration logs (a csv file inside the `DATA` folder) with the following columns: board (bpod name as 
it is inside PyBpod), date, target (in microliters), pulse-duration (the final pulse durations), water-g (the final result), cycles (the number
of averaged pulses) and tolerance (in microliters). If the calibration is not reached or the program is stopped while calibrating, all data is lost and 
no entry is saved.

![waterCalibrationPlugin home](images/water-cal-window.png)

The plugin is tested on Ubuntu with Python 3 and the latest (6/6/2019) PyBpod version.

# Caveats and problems:
There are two bugs affecting calibrations which are run for the first time:

 1. There's a small bug with the current pybpod version: when creating new tasks, it gives a UUID error the first time that the new task is run. Since the plugin will create 
    a new task for boards that have never been calibrated, be aware that this error will pop up. The current solution is to close pybpod and to start it again.

 2. A similar bug is encountered also the first time that a calibration is run, because setups require a subject. The current solution is to run the calibration once,
    stop it immediately, and manually add a subject to the newly created setup.


## How to install:

1. Move the plugin to the location where the rest of the plugins are installed.
2. Navigate to the plugin folder and install it (inside the appropriate python environment, if that's the case) using `python install -e .`. This will
install the plugin with pip in local mode. 
3. Open PyBpod.
4. Select Options > Edit user settings.
5. Add the following info:

```python
    GENERIC_EDITOR_PLUGINS_PATH = 'PATH TO PLUGINS FOLDER' # USE DOUBLE SLASH FOR PATHS ON WINDOWS
    GENERIC_EDITOR_PLUGINS_LIST = [
        (...other plugins...),
        'water_calibration_plugin',
    ]
```
6. Save.
7. Restart PyBpod.
8. Select Tools and open the plugin.


### Buttons:
   - Run: run the calibration.
   - Stop: stop the calibration; no data is saved and all parameter values return to defaults.
   - Send: advance to the next calibration step.

## Developer info:

The core of the plugin is a state machine which compares the water results with the target, and keeps adjusting the pulse duration until it is reached.
The biggest part of the code is inside `water_windows.py`, including the state machine definition. When a previous calibration has already been made,
the program reads the csv log inside DATA, recovers the values and starts the new calibration from there. The variable which links log entries to the correct bpods
is the board name, so be careful if the names change for some reason.

When a new board has to go through the first calibration, the program will start from a set of default values inside `default_values.py`. Usually the first default value will be quite low
when compared with the target, and the pulse duration will keep increasing until the result surpasses the target. This can cause a problem because different setups use different valves (ask for
valves vs pumps to the technician) and the pulse durations can be different by a whole order of magnitude (7 ms for bigger pumps vs 120 ms for smaller valves are typical values), so the calibration can
be too long. Feel free to change the default values depending on the valve type, and also the parameter which sets the amount of time increase/decrease, by editing the return parameters inside method `get_delta` inside `water_windows.py`. 

Once the first calibration is done, the results will be saved and calibrations should be much faster from that point onwards.
  
   
## License
This is Open Source software. We use the `GNU General Public License version 3 <https://www.gnu.org/licenses/gpl.html>`


  


